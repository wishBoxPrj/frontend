import React, {Component} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Autocomplete from '@material-ui/lab/Autocomplete';
import StarRatings from 'react-star-ratings';
import TextField from '@material-ui/core/TextField';
import Cookie from 'js-cookie';
import { PrivateInstance } from './helpers/PrivateInstance';
import Snackbar from '@material-ui/core/Snackbar';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import EditWishIcon from './icons/EditWishIcon';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#E25781'
        },
    },
});

class WishEdit extends Component {
    constructor(props){
        super(props);
        this.state ={
            isOpen:false,
            rating:1,
            openAlert: false
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.getCategories = this.getCategories.bind(this);
        this.changeRating = this.changeRating.bind(this);
        this.wishInputHandleChange = this.wishInputHandleChange.bind(this);
        this.additionalInputHandleChange = this.additionalInputHandleChange.bind(this);
        this.handleCloseAlert = this.handleCloseAlert.bind(this);
        this.patchWish = this.patchWish.bind(this);
        this.getCategories();
    }

    handleOpen = () =>{
        this.setState({
            isOpen:true
        });
    }

    getCategories(){
        let url = 'https://dev.backend.powerwishes.me/gifts/categories';
        PrivateInstance().get('/gifts/categories')
            .then((categoriesResponse) => {
                this.setState({
                    categoriesList: categoriesResponse.data
                });
            });
    }

    patchWish(){
        let url = `https://dev.backend.powerwishes.me/gifts/${this.props.wishId}`;
        let body = {
            name:this.state.user_wish,
            comment:this.state.wish_additional,
            category:this.state.wish_category,
            priority:this.state.rating
        };
        PrivateInstance().patch(`/gifts/${this.props.wishId}`, body)
            .then((responseWish) => {
                this.setState({
                    isOpen:false
                });
            })
            .catch((error) => {
                this.setState({
                    openAlert: true
                });
            });
    }


    handleClickOpen = () => {
        this.setState({
            isOpen:true
        });
    };

    handleClose = () => {
        this.setState({
            isOpen:false
        });
    };

    handleCloseAlert = () => {
        this.setState({
            openAlert:false
        });
    };


    changeRating( newRating, name ) {
        this.setState({
            rating: newRating
        });
    }

    wishInputHandleChange(event) {
        this.setState({
            user_wish: event.target.value
        });
    }

    additionalInputHandleChange(event) {
        this.setState({
            wish_additional: event.target.value
        });
    }

    render(){
        return(
            <MuiThemeProvider theme = {theme}>
                <div>
                    <button className = 'wishList-button' style = {{marginTop:'5px'}} onClick = {this.handleOpen}>
                        <EditWishIcon style = {{width:'25px', height:'25px', position:'relative', top:'3px'}}/>
                    </button>
                    <Dialog
                        open = {this.state.isOpen}
                        style = {{marginBottom:'200px'}}
                    >
                        <DialogContent style = {{display:'flex', flexDirection:'column', background:'#FFE3E8', alignItems:'center'}}>
                            <DialogContentText style = {{fontSize:'28px', color:'424242',marginRight:'450px'}}>
                  Edit
                            </DialogContentText>
                            <TextField label = 'Wish*' style = {{width:'500px'}} placeholder = 'Enter your wish' onChange = {this.wishInputHandleChange}/>
                            <TextField label = 'Description' style = {{width:'500px'}} placeholder = 'Enter additional info about you wish' onChange = {this.additionalInputHandleChange}/>
                            <Autocomplete
                                onChange={(event, value) => this.setState({wish_category:value})}
                                options={this.state.categoriesList}
                                getOptionLabel={option => option}
                                style={{ width: 300,marginTop: 25 }}
                                renderInput={params => (
                                    <TextField {...params} label='Category' variant='outlined' fullWidth/>
                                )}
                            />
                            <div style = {{position:'relative', top:'15px'}}>
                                <StarRatings
                                    rating={this.state.rating}
                                    starRatedColor='E25781'
                                    starHoverColor = 'E25781'
                                    starEmptyColor = 'E5CCD1'
                                    changeRating={this.changeRating}
                                    numberOfStars={5}
                                    name='mywish'
                                    starDimension = '30px'
                                />
                            </div>
                            <div className = 'flexRow' style = {{marginTop:'20px', marginLeft:'350px', marginBottom:'10px',justifyContent:'space-between'}}>
                                <button className = 'dialog-button' onClick = {this.handleClose}>CANCEL</button>
                                <button className = 'dialog-button' onClick = {this.patchWish}>DONE</button>
                                { this.state.openAlert ?
                                    <Snackbar
                                        anchorOrigin = {{ vertical: 'top', horizontal: 'center' }}
                                        open = {this.state.openAlert}
                                        autoHideDuration = {10000}
                                        onClose = {this.handleCloseAlert}
                                        variant = 'primary'
                                        message={<span>You forgot to choose the wish Category</span>}
                                    />
                                    : null
                                }
                            </div>
                        </DialogContent>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
}
export default WishEdit;
