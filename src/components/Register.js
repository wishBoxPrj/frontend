import React from 'react';
import axios from 'axios';

class Register extends React.Component {

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange() {
        window.open('https://oauth.vk.com/authorize?client_id=7148970&display=page&redirect_uri=https://dev.powerwishes.me/handle&response_type=code','_self');
    }

    render () {
        return (
            <div className = 'sign-in-background'>
                <img src='logo.png' width = '321' height = '125' className = 'logo' style = {{paddingBottom: '125px'}}/>
                <button className = 'sign-in-button' onClick = {this.handleChange}>
                    <i className = 'fa fa-vk fa-2x' style = {{position:'absolute', right:'250px', top:'6.5px'}}/>
                    Sign in via VKontakte
                </button>
            </div>
        );
    }
}


export default Register;
