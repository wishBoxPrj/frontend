import React from 'react';
import FriendList from './FriendList';
import FriendRequest from './FriendRequest';
import FriendSearch from './FriendSearch';
import FriendWishList from './FriendWishList';
import { Tabs, Tab, TabPanel, TabList } from 'react-web-tabs';
import 'react-web-tabs/dist/react-web-tabs.css';

class Friend extends React.Component {
    constructor(props) {
        super(props);
        this.state = { };
        this.openWishList = this.openWishList.bind(this);
    }

    openWishList(nickname){
    }

    render(){
        return (
            <div>
                <div className = 'friends-background'>
                    <label className = 'friends-title'> Friends </label>
                    <Tabs
                        style = {{paddingTop:'30px'}}
                        onChange={() => this.setState()}
                    >
                        <TabList>
                            <Tab tabFor="one">All</Tab>
                            <Tab tabFor="two">Requests</Tab>
                            <Tab tabFor="three">Find</Tab>
                        </TabList>
                        <TabPanel tabId="one">
                            <FriendList className = 'friends-container' openWishList = {this.openWishList}/>
                        </TabPanel>
                        <TabPanel tabId="two">
                            <FriendRequest/>
                        </TabPanel>
                        <TabPanel tabId="three">
                            <FriendSearch/>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        );
    }
}

export default Friend;
