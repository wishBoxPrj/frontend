import React from 'react';
import queryString from 'query-string';
import axios from 'axios';
import Cookie from 'js-cookie';
import { PrivateInstance } from './helpers/PrivateInstance';

class TokenHandle extends React.Component {

    constructor(props) {
        super(props);
        this.tokenPost = this.tokenPost.bind(this);
        this.tokenPost();
    }

    tokenPost(accessCode) {
        let authForm = {
            'code': queryString.parse(this.props.location.search).code,
            'provider': 'vk-oauth2',
            'redirect_uri': 'https://dev.powerwishes.me/handle'
        };

        PrivateInstance().post ('/login/social/token/', authForm)
            .then((response) => {
                axios.get('https://dev.backend.powerwishes.me/profiles/me',
                    {headers: {'Authorization': `Token ${response.data.token}`,'Content-Type': 'application/json'}} )
                    .then((profileResponse) => {
                        if(profileResponse.data.nickname) {
                            Cookie.set('token', `Token ${response.data.token}`);
                            Cookie.set('nickname',profileResponse.data.nickname);
                            this.props.history.push('/friends');
                        } else {
                            sessionStorage.setItem('token',`Token ${response.data.token}`);
                            this.props.history.push('/auth-complete');
                        }
                    });

            })
            .catch((error) => {
            });
    }
    render(){
        return (
            <div className = 'main-background' style = {{fontSize: '16px'}}>Обработка данных ...</div>
        );
    }
}

export default TokenHandle;
