import React from 'react';
import axios from 'axios';
import Cookie from 'js-cookie';

export const PrivateInstance = () => {
    const cookieToken = Cookie.get('token');
    const nickname = Cookie.get('nickname');
    return axios.create({
        baseURL: 'https://dev.backend.powerwishes.me',
        headers: {'Authorization': `${cookieToken}`,'Content-Type': 'application/json'}
    }
    );
};
