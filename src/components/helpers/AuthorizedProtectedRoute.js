import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import Cookie from 'js-cookie';

export const AuthorizedProtectedRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            Cookie.get('token') ? (
                <Redirect
                    to={{pathname:'/friends', state:{from: props.location}}}
                />
            ) : (
                <Component {...props} />
            )
        }
    />
);
