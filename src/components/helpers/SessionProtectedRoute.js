import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import Cookie from 'js-cookie';

export const SessionProtectedRoute = ({component: Component, ...rest}) => (
    <Route
        {...rest}
        render={props =>
            sessionStorage.getItem('token') ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{pathname:'/login', state:{from: props.location}}}
                />
            )
        }
    />
);
