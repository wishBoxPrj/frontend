import React from 'react';
import Cookie from 'js-cookie';
import Avatar from 'react-avatar';
import * as moment from 'moment';
import CircularProgress from '@material-ui/core/CircularProgress';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { PrivateInstance } from './helpers/PrivateInstance';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#E25781'
        },
    },
});

class FriendList extends React.Component {
    constructor(props){
        super(props);
        this.state = { };
        this.getUserFriendsInfo = this.getUserFriendsInfo.bind(this);
        this.getUserFriendsInfo();
    }

    getUserFriendsInfo() {
        PrivateInstance().get('/friendship/friends')
            .then((response) => {
                this.setState({
                    friendsList:response.data
                });
            });
    }

    render(){
        return(
            <MuiThemeProvider theme = {theme}>
                <div className = 'friends-container'>
                    {this.state.friendsList ?
                        this.state.friendsList.map(friend =>(
                            <div key = {friend.owner_id}>
                                <label className = 'friend-date-of-birth'>{moment(friend.date_of_birth).format('ddd D MMM')}</label>
                                <div className = 'friends-list' style = {{marginTop:'2px'}} onClick = {
                                    () =>{this.props.openWishList(friend.nickname,this.state.isClicked);}
                                }>
                                    <Avatar
                                        name = {`${friend.owner.first_name} ${friend.owner.last_name}`}
                                        size = '37.5'
                                        round = '75px'
                                        color = {'linear-gradient(100deg, #ff9e81 7.5%, #e25781 100%)'}
                                        className = 'avatar-font'
                                    />
                                    {friend.owner.first_name} {friend.owner.last_name}
                                </div>
                            </div>
                        ))
                        : <div style = {{paddingLeft:'120px'}}>
                            <CircularProgress color='primary'/>
                        </div>
                    }
                </div>
            </MuiThemeProvider>
        );
    }
}

export default FriendList;
