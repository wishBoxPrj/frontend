import React from 'react';
import { Link } from 'react-router-dom';

class NotFound extends React.Component {

    render(){
        return (
            <div className = 'main-background'>
                <h1 className = 'not-found-title' style = {{fontSize: '404px'}}>404</h1>
                <p className = 'not-found-text' style = {{fontSize: '30px'}}>OOOOOOOOOOOOOPS! Something went wrong...</p>
                <Link to = '/'><button className = 'not-found-button'>Go to home page</button></Link>
            </div>
        );
    }
}

export default NotFound;
