import React from 'react';
import Cookie from 'js-cookie';
import SearchIcon from '@material-ui/icons/Search';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import Avatar from 'react-avatar';
import AddFriendRequestIcon from './icons/AddFriendRequestIcon';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { PrivateInstance } from './helpers/PrivateInstance';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: 'rgba(0,0,0,0.35)'
        },

        secondary: {
            main: '#E25781'
        }
    },
    props: {
        MuiButtonBase: {
            disableRipple: true
        }
    }
});

class FriendSearch extends  React.Component {
    constructor(props){
        super(props);
        this.state = { };
        this.findUser = this.findUser.bind(this);
        this.inputHandleChange = this.inputHandleChange.bind(this);
        this.keyInput = this.keyInput.bind(this);
        this.sendFriendRequest = this.sendFriendRequest.bind(this);
    }

    findUser(){
        PrivateInstance().get(`profiles?nickname=${this.state.user_nickname}`)
            .then((searchResponse) => {
                this.setState({
                    friendsSearchList: searchResponse.data
                });
                this.state.friendsSearchList.map(friend_id =>
                    this.setState({
                        search_friend_nickname: friend_id.nickname
                    }));
            });
    }

    inputHandleChange(event) {
        this.setState({
            user_nickname: event.target.value
        });
    }

    keyInput(event){
        if(event.keyCode == 13){
            this.findUser();
        }
    }

    sendFriendRequest() {
        let body = {nickname:this.state.search_friend_nickname};
        PrivateInstance().post('/friendship/add')
            .then((requestResponse) => {
            });
    }

    render(){
        return(
            <MuiThemeProvider theme = {theme}>
                <div className = 'flexColumn'>
                    <div className = 'flexRow' style = {{paddingLeft:'15px', width:'100%'}}>
                        <TextField
                            label = 'Search'
                            onChange = {this.inputHandleChange}
                            onKeyDown = {this.keyInput}
                            color = 'primary'
                            style = {{width:'400px'}}
                            placeholder = 'Enter your friends nickname'
                        />
                        <IconButton
                            style = {{width:'30px',height:'30px',position:'relative',top:'17px', right:'30px'}}
                            onClick = {this.findUser}
                        >
                            <SearchIcon style = {{width:'23px',height:'23px', position: 'relative', bottom:'9px'}}/>
                        </IconButton>
                    </div>
                    {this.state.friendsSearchList ?
                        this.state.friendsSearchList.map(search_id => (
                            <div key = {search_id.owner_id} className = 'friends-list' style = {{paddingLeft:'30px'}}>
                                <Avatar
                                    name = {`${search_id.owner.first_name} ${search_id.owner.last_name}`}
                                    size = '37.5'
                                    round = '75px'
                                    color = {'linear-gradient(100deg, #ff9e81 7.5%, #e25781 100%)'}
                                    className = 'avatar-font'
                                />
                                {search_id.owner.first_name} {search_id.owner.last_name}
                                <button className = 'response-request-button' onClick = {this.sendFriendRequest}><AddFriendRequestIcon/></button>
                            </div>
                        )): null
                    }
                </div>
            </MuiThemeProvider>
        );
    }
}

export default FriendSearch;
