import React, {Component} from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Autocomplete from '@material-ui/lab/Autocomplete';
import StarRatings from 'react-star-ratings';
import TextField from '@material-ui/core/TextField';
import MagicStickIcon from './icons/MagicStickIcon';
import Cookie from 'js-cookie';
import { PrivateInstance } from './helpers/PrivateInstance';
import Snackbar from '@material-ui/core/Snackbar';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#E25781'
        },
    },
});

class WishCreate extends Component {
    constructor(props){
        super(props);
        this.state ={
            isOpen:false,
            rating:1,
            openAlert: false
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.getCategories = this.getCategories.bind(this);
        this.changeRating = this.changeRating.bind(this);
        this.wishInputHandleChange = this.wishInputHandleChange.bind(this);
        this.additionalInputHandleChange = this.additionalInputHandleChange.bind(this);
        this.postWish = this.postWish.bind(this);
        this.handleCloseAlert = this.handleCloseAlert.bind(this);
        this.getCategories();
    }

    getCategories(){
        PrivateInstance().get('/gifts/categories')
            .then((categoriesResponse) => {
                this.setState({
                    categoriesList: categoriesResponse.data
                });
            });
    }

    postWish(){
        const nickname = Cookie.get('nickname');
        let body = {
            name:this.state.user_wish,
            comment:this.state.wish_additional,
            category:this.state.wish_category,
            priority:this.state.rating
        };
        PrivateInstance().post(`/gifts/${nickname}`, body)
            .then((responseWish) => {
                this.setState({
                    isOpen:false
                });
            })
            .catch((error) => {
                this.setState({
                    openAlert: true
                });
            });
    }

    handleClickOpen = () => {
        this.setState({
            isOpen:true
        });
    };

    handleClose = () => {
        this.setState({
            isOpen:false
        });
    };

    handleCloseAlert = () => {
        this.setState({
            openAlert:false
        });
    };


    changeRating( newRating, name ) {
        this.setState({
            rating: newRating
        });
    }

    wishInputHandleChange(event) {
        this.setState({
            user_wish: event.target.value
        });
    }

    additionalInputHandleChange(event) {
        this.setState({
            wish_additional: event.target.value
        });
    }

    render(){
        return(
            <MuiThemeProvider theme = {theme}>
                <div>
                    <button className = 'wish-button' style = {{marginTop:'25px'}} onClick = {this.handleClickOpen}> Make a Wish <MagicStickIcon style = {{width:'18px', height:'18px', position:'relative', top:'2px',left:'15px'}}/></button>
                    <Dialog
                        open = {this.state.isOpen}
                        style = {{marginBottom:'200px'}}
                    >
                        <DialogContent style = {{display:'flex', flexDirection:'column', background:'#FFE3E8', alignItems:'center'}}>
                            <DialogContentText style = {{fontSize:'28px', color:'424242',marginRight:'350px'}}>
                Make a wish
                            </DialogContentText>
                            <TextField label = 'Wish*' style = {{width:'500px'}} placeholder = 'Enter your wish' onChange = {this.wishInputHandleChange}/>
                            <TextField label = 'Description' style = {{width:'500px'}} placeholder = 'Enter additional info about you wish' onChange = {this.additionalInputHandleChange}/>
                            <Autocomplete
                                onChange={(event, value) => this.setState({wish_category:value})}
                                id='combo-box-demo'
                                options={this.state.categoriesList}
                                getOptionLabel={option => option}
                                style={{ width: 300,marginTop: 25 }}
                                renderInput={params => (
                                    <TextField {...params} label='Category' variant='outlined' fullWidth/>
                                )}
                            />
                            <div style = {{position:'relative', top:'15px'}}>
                                <StarRatings
                                    rating={this.state.rating}
                                    starRatedColor='E25781'
                                    starHoverColor = 'E25781'
                                    starEmptyColor = 'E5CCD1'
                                    changeRating={this.changeRating}
                                    numberOfStars={5}
                                    name='mywish'
                                    starDimension = '30px'
                                />
                            </div>
                            <div className = 'flexRow' style = {{marginTop:'20px', marginLeft:'350px', marginBottom:'10px',justifyContent:'space-between'}}>
                                <button className = 'dialog-button' onClick = {this.handleClose}>CANCEL</button>
                                <button className = 'dialog-button' onClick = {this.postWish}>DONE</button>
                                { this.state.openAlert ?
                                    <Snackbar
                                        anchorOrigin = {{ vertical: 'top', horizontal: 'center' }}
                                        open = {this.state.openAlert}
                                        autoHideDuration = {10000}
                                        onClose = {this.handleCloseAlert}
                                        variant = 'primary'
                                        message={<span>You forgot to choose the wish Category</span>}
                                    />
                                    : null
                                }
                            </div>
                        </DialogContent>
                    </Dialog>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default WishCreate;
