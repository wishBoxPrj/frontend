import React, {Component} from 'react';
import WishCreate from './WishCreate';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import Table from 'react-bootstrap/Table';
import StarRatings from 'react-star-ratings';
import EditWishIcon from './icons/EditWishIcon';
import DeleteWishIcon from './icons/DeleteWishIcon';
import Cookie from 'js-cookie';
import WishEdit from './WishEdit';
import { PrivateInstance } from './helpers/PrivateInstance';

class WishlistList extends Component {
    constructor(props){
        super(props);
        this.state = { };
        this.wishDelete = this.wishDelete.bind(this);
    }

    componentWillMound(){
        this.wishMap();
    }

    wishDelete(){


    }

    render(){
        return(
            <div className = 'flexColumn' style = {{height:'100vh', marginLeft:'90px'}}>
                <div className = 'flexRow' style = {{justifyContent:'space-between'}}>
                    <div className = 'lato-font' style = {{fontSize:'40px', color:'rgba(0,0,0,0.35)', marginTop:'30px'}}> My wish list </div>
                    <div style = {{marginTop:'16px', marginRight:'700px'}}>
                        <WishCreate style = {{position:'relative', top:'30px'}}/>
                    </div>
                    <div>
                        <TextField
                            label = 'Search'
                            onChange = {this.inputHandleChange}
                            onKeyDown = {this.keyInput}
                            color = 'primary'
                            style = {{width:'320px', marginTop:'22px'}}
                            placeholder = 'Enter your friends nickname'
                        />
                        <IconButton
                            style = {{width:'30px',height:'30px',position:'relative',top:'42px', right:'30px'}}
                            onClick = {this.findUser}
                        >
                            <SearchIcon style = {{width:'23px',height:'23px', position: 'relative', bottom:'9px'}}/>
                        </IconButton>
                    </div>
                </div>
                <div className = 'lato-font' style = {{color:'#D7859B', fontSize:'20px', fontWeight:'bold', marginTop:'20px', marginBottom:'15px'}}>
                    <div className = 'flexRow' style = {{marginLeft:'20px'}}>
                        <div style = {{width:'250px'}}> Wish </div>
                        <div style = {{width:'650px'}}> Description </div>
                        <div style = {{width:'250px'}}> Priority </div>
                        <div style = {{width:'250px'}}> Category </div>
                        <div style = {{width:'125px'}}/>
                    </div>
                </div>
                {
                    this.props.wishList.map(wish_id => (
                        <div key = {wish_id.id} className = 'flexRow' style = {{background: 'rgba(255, 255, 255, 0.4)', borderRadius:'5px', marginBottom:'8px', height:'60px', alignItems:'center', marginRight:'20px',justifyContent:'space-between'}}>
                            <div style = {{width:'230px', paddingLeft:'20px'}}>{wish_id.name}</div>
                            <div style = {{width:'620px'}}>{wish_id.comment}</div>
                            <div style = {{width:'225px'}}>
                                <StarRatings
                                    rating = {wish_id.priority}
                                    numberOfStars={wish_id.priority}
                                    starRatedColor='E25781'
                                    starDimension = '15px'
                                />
                            </div>
                            <div style = {{width:'255px'}}>{wish_id.category}</div>
                            <div className = 'flexRow' style = {{width:'80px'}}>
                                <WishEdit wishId = {wish_id.id}/>
                                <button className = 'wishList-button' style = {{marginTop:'5px'}} onClick = {() =>{
                                    PrivateInstance().delete(`/gifts/${wish_id.id}`)
                                        .then((response) => {
                                        });
                                }}><DeleteWishIcon style = {{width:'20px', height:'20px'}}/></button>
                            </div>
                        </div>
                    ))
                }
            </div>
        );
    }
}

export default WishlistList;
