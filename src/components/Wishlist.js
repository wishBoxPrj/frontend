import React from 'react';
import Button from '@material-ui/core/Button';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Cookie from 'js-cookie';
import { PrivateInstance } from './helpers/PrivateInstance';
import WishListPresentIcon from './icons/WishListPresentIcon';
import PlusIcon from './icons/PlusIcon';
import WishlistList from './WishlistList';
import WishCreate from './WishCreate';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: 'rgba(0,0,0,0.35)'
        },

        secondary: {
            main: '#E25781'
        }
    },
    props: {
        MuiButtonBase: {
            disableRipple: true
        }
    }
});

class Wishlist extends React.Component {
    constructor(props){
        super(props);
        this.state = { };
        this.getWishListInfo = this.getWishListInfo.bind(this);
        this.getWishListInfo();
    }

    getWishListInfo(){
        const nickname = Cookie.get('nickname');
        let url = `https://dev.backend.powerwishes.me/gifts/${nickname}`;
        PrivateInstance().get(`/gifts/${nickname}`)
            .then((response) => {
                if( response.data.length != 0){
                    this.setState({
                        wishlistIsEmpty: true,
                        user_wishlist: response.data
                    });

                } else {
                    this.setState({
                        wishlistIsEmpty: false,
                    });
                }

            });
    }

    render(){
        return (
            <MuiThemeProvider theme = {theme}>
                {this.state.wishlistIsEmpty ?
                    <WishlistList wishList={this.state.user_wishlist}/>
                    : <div className = 'flexColumn' style = {{height:'100vh', marginLeft:'90px', alignItems:'center'}}>
                        <div className = 'lato-font' style = {{fontSize:'40px', color:'rgba(0,0,0,0.35)', marginTop:'30px', marginRight:'1300px'}}> My wish list </div>
                        <div style = {{marginTop:'150px'}}>
                            <WishListPresentIcon style = {{width:'200px', height:'200px'}}/>
                            <PlusIcon style = {{height:'55px', width:'55px', position:'relative', right:'15px'}}/>
                        </div>
                        <div className = 'lato-font' style = {{color:'rgba(0,0,0,0.35)', fontSize:'20px', marginTop:'25px'}}> Your wish list is empty. Just make a wish. </div>
                        <WishCreate/>
                    </div>
                }
            </MuiThemeProvider>
        );
    }
}

export default Wishlist;
