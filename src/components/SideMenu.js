import React from 'react';
import { Link, withRouter} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import FriendsIcon from './icons/FriendsIcon';
import WishListIcon from './icons/WishListIcon';
import PresentIcon from './icons/PresentIcon';
import Cookie from 'js-cookie';
import Avatar from 'react-avatar';
import { PrivateInstance } from './helpers/PrivateInstance';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#E25781'
        },
    },
});

class SideMenu extends React.Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.clearToken = this.clearToken.bind(this);
        this.getUserName = this.getUserName.bind(this);
        this.getUserName();
        this.state = {
            name: null
        };
        switch (window.location.pathname) {
        case '/profile':
            this.state = {page:0};
            break;
        case '/mywishlist':
            this.state = {page:1};
            break;
        case '/friends':
            this.state = {page:2};
            break;
        }
    }

    getUserName() {
        PrivateInstance().get('/profiles/me')
            .then((response) => {
                this.setState({
                    name:`${response.data.owner.first_name } ${response.data.owner.last_name }`
                });
            });
    }

    handleChange (event, newValue) {
        this.setState({page:newValue});
        switch (newValue) {
        case 0:
            this.props.history.push('/profile');
            break;
        case 1:
            this.props.history.push('/mywishlist');
            break;
        case 2:
            this.props.history.push('/friends');
            break;
        }
    }

    clearToken () {
        Cookie.remove('token');
        Cookie.remove('nickname');
    }

    render(){
        return (
            <MuiThemeProvider theme = {theme}>
                <div className = 'tabbar-background'>
                    <div className = 'flexColumn'>
                        <div className = 'sidebar'>
                            <Tabs style = {{width:'80px', height:'100%'}}
                                value = {this.state.page}
                                onChange={this.handleChange}
                                orientation = 'vertical'
                                indicatorColor='primary'
                                textColor='primary'
                                centered
                            >
                                <Tab style = {{minWidth:'100px',position: 'relative', top:'20px', paddingRight:'16.9px'}}
                                    icon={<Avatar name={this.state.name} size='50' round = '75px' color={'linear-gradient(100deg, #ff9e81 7.5%, #e25781 100%)'}/>}
                                />
                                <Tab style = {{minWidth:'100px',position: 'relative', right:'12px',top:'250px', marginBottom: '50px'}} icon={<WishListIcon/>}/>
                                <Tab style = {{minWidth:'100px',position: 'relative', right:'14px',top:'250px', marginBottom: '50px'}} icon={<FriendsIcon style = {{position:'relative', bottom: '4px'}}/>}/>}/>
                                <Tab style = {{minWidth:'100px',position: 'relative', right:'13px',top:'250px', marginBottom: '50px'}} icon={<PresentIcon style = {{position:'relative', bottom: '4px'}}/>}/>
                            </Tabs>
                            <Link to = '/'><Button color = 'primary' style = {{height: '50px', width: '80px', position:'relative'}} onClick = {this.clearToken}> <img src = '/logouticon.svg' style = {{width: '20px', height: '20px'}}/></Button></Link>
                        </div>
                        {this.props.children}
                    </div>
                </div>
            </MuiThemeProvider>
        );
    }
}

export default withRouter(SideMenu);
