import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import * as moment from 'moment';
import axios from 'axios';
import Cookie from 'js-cookie';

class AuthComplete extends React.Component {

    constructor(props) {
        super(props);
        this.completeAuth = this.completeAuth.bind(this);
        this.inputChange = this.inputChange.bind(this);
        this.state = {
            startDate:  null,
            nicknameInput: ''
        };
    }

    completeAuth () {
        const sessionToken = sessionStorage.getItem('token');
        let additionalInfo = {
            'nickname': this.state.nicknameInput,
            'date_of_birth': moment(this.state.startDate).format('YYYY-MM-DD'),
            'photo': null
        };
        let patchHeaders = {'Authorization': `${sessionToken}`,'Content-Type': 'application/json'};

        axios.patch('https://dev.backend.powerwishes.me/profiles/me', additionalInfo, {headers: patchHeaders})
            .then((response) => {
                Cookie.set('token', sessionToken);
                Cookie.set('nickname',response.data.nickname);
                this.props.history.push('/friends');
            });
    }

    inputChange (event) {
        this.setState({
            nicknameInput: event.target.value
        });
    }

    handleChange = date => {
        this.setState({
            startDate: date
        });
    };

    render () {
        return (
            <div className = 'sign-in-background'>
                <img src='logo.png' width = '321' height = '125' className = 'logo' />
                <form className = 'auth-group'>
                    <input type = 'text' placeholder = 'Enter your username' className = 'name-input' value = {this.state.nicknameInput} onChange = {this.inputChange}/>
                    <DatePicker  className = 'custom-datepicker'
                        dateFormat = 'dd.MM.yyyy'
                        showYearDropdown
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                        placeholderText = 'Enter your birth date'
                        dropdownMode = 'scroll'
                        minDate = {new Date ('01.01.1960')}
                        maxDate = {new Date ('01.1.2008')}
                    />
                    <input type = 'button' value = "Let's start" className = 'info-submit' onClick = {this.completeAuth}/>
                </form>
            </div>
        );
    }
}

export default AuthComplete;
