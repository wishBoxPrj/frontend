import React from 'react';
import Cookie from 'js-cookie';
import DatePicker from 'react-datepicker';
import Avatar from 'react-avatar';
import IconVK from './icons/IconVK';
import EditDateIcon from './icons/EditDateIcon';
import * as moment from 'moment';
import { PrivateInstance } from './helpers/PrivateInstance';

class Profile extends React.Component {

    constructor(props){
        super(props);
        this.getProfileInfo = this.getProfileInfo.bind(this);
        this.state = {
            nickname: null,
            birthDate: null,
            name: null
        };
        this.getProfileInfo();
    }

    getProfileInfo(){
        PrivateInstance().get('profiles/me')
            .then((response) => {
                this.setState({
                    nickname: `@${response.data.nickname}`,
                    birthDate: new Date(response.data.date_of_birth),
                    name:`${response.data.owner.first_name } ${response.data.owner.last_name }`
                });
            });
    }

    handleChange = date => {
        this.setState({
            birthDate: date
        });
    };

    render(){
        return (
            <div className = 'profile-page'>
                <label style = {{color: '#424242', paddingTop:'20px'}}>{this.state.nickname}</label>
                <Avatar style = {{margin: '15px'}}
                    name={this.state.name}
                    size='150px'
                    round = '500px'
                    color={'linear-gradient(100deg, #ff9e81 7.5%, #e25781 100%)'}
                />
                <label style = {{color: '#424242', marginBottom:'30px', fontSize:'18px'}}> {this.state.name}
                    <IconVK style ={{width:'16px', height:'16px', position:'relative', left:'10px', top:'3px'}}/>
                </label>
                <label style = {{height:'40px', width:'290px'}}>
                    <DatePicker  className = 'custom-datepicker'
                        dateFormat = 'dd.MM.yyyy'
                        showYearDropdown
                        selected={this.state.birthDate}
                        onChange={this.handleChange}
                        placeholderText = 'Enter your birth date'
                        dropdownMode = 'scroll'
                        minDate = {new Date ('01.01.1960')}
                        maxDate = {new Date ('01.1.2008')}
                    /><EditDateIcon style = {{width:'16px', height:'16px',position:'relative', left:'260px', bottom:'55px'}}/>
                </label>
            </div>
        );
    }
}

export default Profile;
