import React from 'react';
import Cookie from 'js-cookie';
import Avatar from 'react-avatar';
import AddFriendRequestIcon from './icons/AddFriendRequestIcon';
import CancelFriendRequestIcon from './icons/CancelFriendRequestIcon';
import {MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { PrivateInstance } from './helpers/PrivateInstance';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: 'rgba(0,0,0,0.35)'
        },
    },
});

class FriendRequest extends React.Component {
    constructor(props){
        super(props);
        this.state = { };
        this.getFriendsRequest = this.getFriendsRequest.bind(this);
        this.getFriendsRequest();
    }

    getFriendsRequest(){
        PrivateInstance().get('/friendship/requests')
            .then((response) => {
                this.setState({
                    friendsRequestList:response.data
                });
                this.state.friendsRequestList.map(friend_id =>
                    this.setState({
                        friend_nickname: friend_id.nickname
                    }));
            });
    }

    render(){
        return(
            <div className = 'friends-container'>
                {this.state.friendsRequestList ?
                    this.state.friendsRequestList.map(friend =>(
                        <div key = {friend.owner_id} className = 'friends-list'>
                            <Avatar
                                name = {`${friend.owner.first_name} ${friend.owner.last_name}`}
                                size = '37.5'
                                round = '75px'
                                color = {'linear-gradient(100deg, #ff9e81 7.5%, #e25781 100%)'}
                                className = 'avatar-font'/>
                            {friend.owner.first_name} {friend.owner.last_name}
                            <div className = 'flexRow' style = {{marginLeft:'20px'}}>
                                <button className = 'response-request-button' onClick = {
                                    PrivateInstance().post('/friendship/response/accept', {nickname:this.state.friend_nickname})
                                        .then((acceptResponse) => {
                                        })
                                }><AddFriendRequestIcon/></button>
                                <button className = 'response-request-button' onClick = {
                                    PrivateInstance().post('/friendship/response/reject', {nickname:this.state.friend_nickname})
                                        .then((declineResponse) => {
                                        })
                                }><CancelFriendRequestIcon/></button>
                            </div>
                        </div>
                    ))
                    : <div style = {{paddingLeft:'120px'}}>
                        <CircularProgress color='primary'/>
                    </div>
                }
            </div>
        );

    }
}

export default FriendRequest;
