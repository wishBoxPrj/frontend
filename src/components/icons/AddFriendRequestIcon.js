import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

let AddFriendRequestIcon = (props) => (
    <SvgIcon {...props} width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="16.5" cy="6.5" r="3.5" stroke="#424242" strokeOpacity="0.6" strokeWidth="2" fill = "none"/>
        <path d="M10 18.25C10 15.9028 11.9028 14 14.25 14H18.75C21.0972 14 23 15.9028 23 18.25C23 18.6642 22.6642 19 22.25 19H10.75C10.3358 19 10 18.6642 10 18.25Z" stroke="#424242" strokeOpacity="0.6" strokeWidth="2" fill = "none"/>
        <rect x="4" y="7" width="2" height="8" rx="1" fill="#424242" fillOpacity="0.6"/>
        <rect x="1" y="10" width="8" height="2" rx="1" fill="#424242" fillOpacity="0.6"/>
    </SvgIcon>
);
AddFriendRequestIcon.displayName = 'AddFriendRequestIcon';
AddFriendRequestIcon.muiName = 'SvgIcon';

export default AddFriendRequestIcon;
