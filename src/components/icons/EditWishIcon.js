import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

let EditWishIcon = (props) => (
    <SvgIcon {...props} width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M22.8282 1.17211C22.0724 0.41625 21.0675 0 19.9986 0C18.9298 0 17.9248 0.41625 17.169 1.17207L5.98562 12.3555C5.89047 12.4507 5.82152 12.5688 5.78543 12.6984L4.02887 19.0093C3.95312 19.2814 4.0302 19.5734 4.23055 19.7726C4.37898 19.9203 4.57789 20 4.78156 20C4.85273 20 4.92453 19.9902 4.99496 19.9702L11.3058 18.1776C11.572 18.102 11.7784 17.8911 11.8483 17.6232C11.9181 17.3554 11.841 17.0706 11.6456 16.8745L7.66699 12.8839L16.9371 3.61371L20.3847 7.06129L13.288 14.1386C12.9825 14.4433 12.9819 14.9379 13.2866 15.2434C13.5912 15.549 14.0859 15.5496 14.3914 15.245L22.8281 6.83121C23.584 6.07539 24.0002 5.07051 24.0002 4.00164C24.0002 2.93277 23.584 1.92789 22.8282 1.17211ZM9.6016 17.0374L5.90754 18.0867L6.94211 14.3698L9.6016 17.0374ZM21.7241 5.72559L21.4911 5.95797L18.042 2.50887L18.2739 2.27691C18.7346 1.81621 19.3471 1.5625 19.9986 1.5625C20.6502 1.5625 21.2626 1.81621 21.7233 2.27695C22.184 2.73762 22.4377 3.35012 22.4377 4.00164C22.4377 4.65316 22.184 5.26566 21.7241 5.72559Z" fill="inherin"/>
        <defs>
            <filter id="filter0_d" x="0" y="0" width="28" height="28" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                <feOffset dy="4"/>
                <feGaussianBlur stdDeviation="2"/>
                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
            </filter>
            <clipPath id="clip0">
            </clipPath>
        </defs>
    </SvgIcon>
);
EditWishIcon.displayName = 'EditWishIcon';
EditWishIcon.muiName = 'SvgIcon';

export default EditWishIcon;
