import React from 'react';
import SvgIcon from '@material-ui/core/SvgIcon';

let EditDateIcon = (props) => (
    <SvgIcon {...props} width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path id="icon/editor/border_color_24px" fill-rule="evenodd" clip-rule="evenodd" d="M13.8067 2.69471C14.0667 2.43474 14.0667 2.01478 13.8067 1.75481L12.2467 0.19497C12.1221 0.070148 11.953 0 11.7767 0C11.6003 0 11.4312 0.070148 11.3067 0.19497L10 1.5015L12.5 4.00124L13.8067 2.69471ZM11.8333 4.66785L9.33334 2.1681L2.76667 8.73409C2.70001 8.80075 2.66667 8.88074 2.66667 8.97407V11.0005C2.66667 11.1872 2.81334 11.3338 3.00001 11.3338H5.02667C5.11334 11.3338 5.20001 11.3005 5.26001 11.2338L11.8333 4.66785ZM14.6667 13.3336H1.33333C0.6 13.3336 0 13.9335 0 14.6668C0 15.4001 0.6 16 1.33333 16H14.6667C15.4 16 16 15.4001 16 14.6668C16 13.9335 15.4 13.3336 14.6667 13.3336Z" fill="white"/>
    </SvgIcon>
);
EditDateIcon.displayName = 'EditDateIcon';
EditDateIcon.muiName = 'SvgIcon';

export default EditDateIcon;
