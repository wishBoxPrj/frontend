import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import SideMenu from './components/SideMenu';
import Register from './components/Register';
import AuthComplete from './components/AuthComplete';
import { PrivateRoute } from './components/helpers/PrivateRoute';
import { SessionProtectedRoute } from './components/helpers/SessionProtectedRoute';
import { AuthorizedProtectedRoute } from './components/helpers/AuthorizedProtectedRoute';
import Friend from './components/Friend';
import NotFound from './components/NotFound';
import Profile from './components/Profile';
import TokenHandle from './components/TokenHandle';
import Wishlist from './components/Wishlist';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Router>
                <Switch>
                    <AuthorizedProtectedRoute exact path='/' component={Register}/>
                    <AuthorizedProtectedRoute exact path='/handle' component={TokenHandle}/>
                    <SessionProtectedRoute exact path='/auth-complete' component={AuthComplete}/>
                    <SideMenu>
                        <PrivateRoute exact path='/friends' component={Friend}/>
                        <PrivateRoute exact path='/profile' component={Profile}/>
                        <PrivateRoute exact path ='/mywishlist' component={Wishlist} />
                    </SideMenu>
                    <Route component={NotFound}/>
                </Switch>
            </Router>
        );
    }
}
export default App;
